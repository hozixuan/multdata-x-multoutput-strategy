from .base import ISaveStrategy
from typing import Any


class SaveAsText(ISaveStrategy):
    def save(self, data: Any, filename: str) -> None:
        with open(f"output/{filename}.txt", "w") as file:
            file.write(str(data))
            print(f"[strt][txt] Saved {type(data).__name__} as Text to {filename}.txt")
