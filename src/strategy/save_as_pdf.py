from typing import Any
import numpy as np

from .base import ISaveStrategy


class SaveAsPDF(ISaveStrategy):
    def save(self, data: Any, filename: str) -> None:
        if isinstance(data, str) or isinstance(data, np.ndarray):
            # Pseudocode for saving as PDF
            with open(f"output/{filename}.pdf", "w") as file:
                file.write(str(data))
            print(f"[strt][pdf] Saving {type(data).__name__} as PDF to {filename}.pdf")
        else:
            raise RuntimeError(
                f"[strt][pdf] Havent implement on saving {type(data).__name__} as PDF"
            )
