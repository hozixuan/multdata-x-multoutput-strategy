from abc import ABC, abstractmethod
from typing import Any


class ISaveStrategy(ABC):
    @abstractmethod
    def save(self, data: Any, filename: str) -> None:
        pass
