import json
import pandas as pd
from typing import Any
from .base import ISaveStrategy


class SaveAsJSON(ISaveStrategy):
    def save(self, data: Any, filename: str) -> None:
        if isinstance(data, pd.DataFrame):
            data.to_json(f"output/{filename}.json")
            print(f"[strt][json] Saved {type(data).__name__} as JSON to {filename}.json")
        else:
            with open(f"output/{filename}.json", "w") as file:
                json.dump(data, file)
                print(f"[strt][json] Saved {type(data).__name__} as JSON to {filename}.json")
