import pandas as pd
from .base import ISaveStrategy


class SaveAsCSV(ISaveStrategy):
    def save(self, data: pd.DataFrame, filename: str) -> None:
        data.to_csv(f"output/{filename}.csv")
        print(f'[strt][csv] Saved {type(data).__name__} as CSV to {filename}.csv')
