import pandas as pd
from .base import ISaveStrategy

class SaveAsExcel(ISaveStrategy):
    def save(self, data: pd.DataFrame, filename: str) -> None:
        data.to_excel(f'output/{filename}.xlsx')
        print(f'[strt][excel] Saved {type(data).__name__} as Excel to {filename}.xlsx')