from typing import Any

from src.strategy.base import ISaveStrategy


class ISaveDecorator(ISaveStrategy):
    def __init__(self, strategy: ISaveStrategy) -> None:
        self._strategy = strategy

    def save(self, data: Any, filename: str) -> None:
        self._strategy.save(data, filename)
