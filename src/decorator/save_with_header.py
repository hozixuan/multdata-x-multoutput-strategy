from typing import Any
import pandas as pd

from src.strategy.base import ISaveStrategy
from .base import ISaveDecorator


class SaveWithHeader(ISaveDecorator):
    def __init__(self, strategy: ISaveStrategy, header: str) -> None:
        super().__init__(strategy)
        self.header = header

    def save(self, data: Any, filename: str) -> None:
        print(f"[deco][header] Adding header: {self.header}")
        if isinstance(data, pd.DataFrame):
            data = data.copy()
            data.columns = [f"{self.header} {col}" for col in data.columns]
        self._strategy.save(data, filename)
