from typing import Any

from src.strategy.base import ISaveStrategy
from .base import ISaveDecorator


class SaveForClient(ISaveDecorator):
    def __init__(self, strategy: ISaveStrategy, client_name: str) -> None:
        super().__init__(strategy)
        self.client_name = client_name

    def save(self, data: Any, filename: str) -> None:
        filename = f'{self.client_name}_{filename}'
        print(f"[deco][client] Adjusting filename for client: {filename}")
        self._strategy.save(data, filename)
