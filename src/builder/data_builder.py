import pandas as pd
import numpy as np

## Interface
from src.data.base import IData
from src.strategy.base import ISaveStrategy


## Data type
from src.data.list_of_ints_data import ListOfIntsData
from src.data.string_data import StringData
from src.data.numpy_data import NumpyArrayData
from src.data.pandas_data import PandasDataFrameData
from src.data.list_of_strings_data import ListOfStringsData

## Decorator
from src.decorator.save_for_client import SaveForClient
from src.decorator.save_with_header import SaveWithHeader


class DataBuilder:
    def __init__(self) -> None:
        self._data = None
        self._strategy = None

    ## Core builder method
    def with_save_strategy(self, strategy: ISaveStrategy) -> "DataBuilder":
        """Set the saving strategy"""
        self._strategy = strategy
        return self

    def build(self) -> IData:
        """Build the data class"""
        if not self._data:
            raise ValueError("Data not created")
        if self._strategy:
            self._data.set_save_strategy(self._strategy)
        return self._data

    ## Data type
    ## All the data type will be added at here

    def create_list_of_ints(self, data: list) -> "DataBuilder":
        self._data = ListOfIntsData(data)
        return self

    def create_list_of_strings(self, data: list) -> "DataBuilder":
        self._data = ListOfStringsData(data)
        return self

    def create_numpy_array(self, data: np.ndarray) -> "DataBuilder":
        self._data = NumpyArrayData(data)
        return self

    def create_pandas_dataframe(self, data: pd.DataFrame) -> "DataBuilder":
        self._data = PandasDataFrameData(data)
        return self

    def create_string(self, data: str) -> "DataBuilder":
        self._data = StringData(data)
        return self

    ## Decorator
    ## All the decorator will be added here
    def for_client(self, client_name: str) -> "DataBuilder":
        if not self._strategy:
            raise ValueError("Save strategy not set")
        self._strategy = SaveForClient(self._strategy, client_name)
        return self

    def with_header(self, header: str) -> "DataBuilder":
        if not self._strategy:
            raise ValueError("Save strategy not set")
        self._strategy = SaveWithHeader(self._strategy, header)
        return self
