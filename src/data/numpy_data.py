import numpy as np
from .base import IData


class NumpyArrayData(IData):
    def __init__(self, data: np.ndarray) -> None:
        super().__init__(data)
