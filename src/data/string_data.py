from .base import IData


class StringData(IData):
    def __init__(self, data: str) -> None:
        super().__init__(data)
