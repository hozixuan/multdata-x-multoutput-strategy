# data/pandas_data.py

import pandas as pd
from .base import IData

class PandasDataFrameData(IData):
    def __init__(self, data: pd.DataFrame) -> None:
        super().__init__(data)
