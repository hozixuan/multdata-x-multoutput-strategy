from .base import IData


class ListOfIntsData(IData):
    def __init__(self, data: list) -> None:
        if not all(isinstance(item, int) for item in data):
            raise ValueError("All elements must be integers")
        super().__init__(data)
