from typing import Any
from abc import ABC, abstractmethod

from src.strategy.base import ISaveStrategy


class IData(ABC):
    def __init__(self, data: Any) -> None:
        self.data = data
        self._save_strategy = None

    def set_save_strategy(self, strategy: ISaveStrategy) -> None:
        self._save_strategy = strategy

    def save(self, filename: str) -> None:
        if not self._save_strategy:
            raise ValueError("Save strategy not set")
        self._save_strategy.save(self.data, filename)
