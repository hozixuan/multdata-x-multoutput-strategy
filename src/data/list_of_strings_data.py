from .base import IData
from typing import List


class ListOfStringsData(IData):

    def __init__(self, data: List[str]) -> None:
        if not all(isinstance(item, str) for item in data):
            raise ValueError("All elements must be strings")
        super().__init__(data)
