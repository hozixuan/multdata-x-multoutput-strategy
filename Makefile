# Makefile with all available commands

TARGETS := $(shell grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | cut -d ':' -f 1)

.PHONY: help $(TARGETS)

help: ## Display this help message
	@echo "Available commands:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

############################################################
## Initialize ##
############################################################

install i:
	@echo "[Development-env] Running initial setup..."
	@pip3 install --upgrade pip
	@pip3 install -r requirements.txt
	@pip3 freeze | grep -v +cpu > requirements.lock.txt
	@echo ""
	@echo "Finished setting up the venv"

run:
	@python3 app.py

false-run:
	@python3 sample_one_script_long.py

clear:
	@rm -ri output/*

clear-force cf:
	@rm -rif output/*
