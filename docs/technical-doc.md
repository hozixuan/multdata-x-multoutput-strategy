# Technical doc

The below are the technical documentaiton.

## Problem Assumption

- Obvious:
  - The developer need to output the same data to multiple format, when there is new export format, it should be easy to add in
  - Easily to add in and maintain the export method
- Hidden:
  - In future, it could have whole new data types that want to output to existing export method, and we hope that we could reuse back the export method
  - Since we may have multiple client, it could be same type of report but having a slight twist to cater for different client taste.

## The goal of this design

1. Easy modify, easy add in, easy extend from it
2. Need not too much prerequistic knowledge to implement, so that speed up from business idea to code implmentation

## Involved Object

Multiple Saving Strategy

- Definition: The final result to output to
- Example: PDF format, CSV, TXT, Word etc.

Multiple Data Incoming

- Definition: The data that gonna be stored
- Example: It could have data such as dataframe, numpy image, list of string, composition of the rest, etc.

Multiple Decoration

- Definition: The data to be preprocess before saving, such as adjust data for the final output, transform from a datatype to another data type.
- Example: The company name for the report, output date, output filename

## Design Pattern

### 1. Strategy Pattern

The strategies module contains various save strategies implementing the SaveStrategy interface. Each strategy is responsible for saving data in a specific format.

- Pros: Promotes separation of concerns and makes it easy to add new saving formats without modifying existing code.
- Cons: Each new format requires a new class, which can lead to an increased number of classes.

### 2. Decorator Pattern

The decorators module provides additional functionalities that can be dynamically added to save strategies, such as adding headers or client-specific filenames.

- Pros: Enhances flexibility and allows behaviors to be added dynamically.
- Cons: Can lead to a complex class hierarchy and increased code complexity.

### 3. Builder Pattern

The builder module includes the DataBuilder class, which simplifies the creation and configuration of data objects with appropriate save strategies and decorators.

- Pros: Simplifies object creation and configuration, making the code easier to manage.
- Cons: The builder pattern can add complexity and might be overkill for simple object creation.

## Pros and Cons

### Pros

- Maintainability: Clear separation of concerns makes the codebase easy to maintain and extend.
- Flexibility: The use of design patterns allows for dynamic configuration and extension of functionalities.
- Scalability: New data types and save formats can be added with minimal changes to existing code.

### Cons

- Complexity: The combination of multiple design patterns can make the codebase complex and harder to understand for new developers.
- Increased Number of Classes: Each new functionality typically requires a new class, which can lead to a bloated class hierarchy.

## Limitations

- Performance Overhead: The use of decorators and multiple layers of abstraction might introduce performance overhead.
- Learning Curve: Understanding and effectively using the design patterns employed requires a good grasp of design principles, which might be challenging for new developers.

## Future Improvements

- Performance Optimization: Investigate and optimize any potential performance bottlenecks introduced by the extensive use of design patterns.
- Dynamic Configuration: Implement a more dynamic configuration system that can read save strategies and decorators from a configuration file, reducing the need for hardcoding.
- Improved Error Handling: Enhance error handling across the system to provide more informative error messages and robust recovery mechanisms.
- Extended Data Types and Formats: Continuously add support for new data types and save formats based on user requirements and industry trends.
- Documentation and Tutorials: Provide detailed documentation and tutorials to help new developers understand and use the system effectively.
