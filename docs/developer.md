# Developer Note

This note provides guidelines for scenarios: adding a new export method and introducing a new data type.
Follow these steps to ensure consistency and maintainability in the codebase.

1. [Scenario 1: Adding a New Save Strategy](#scenario-1-adding-a-new-save-strategy)
2. [Scenario 2: Adding a New Data Type](#scenario-2-adding-a-new-data-type)
3. [Scenario 3: Adding a New Decorator](#scenario-3-adding-a-new-decorator)

## Scenario 1: Adding a New Save Strategy

Save strategies determine how data is saved to a file, and each strategy is responsible for a specific format, such as PDF, Excel, or Text.

1. Create a New Strategy Class:

   - Navigate to the `strategy` folder.
   - Create a new Python file for the strategy. Name the file according to the functionality of the strategy, e.g., `save_as_html.py`.

2. Implement the New Strategy:

   - Define a new class in the created file.
   - Ensure that the new strategy class inherits from `ISaveStrategy`.
   - Implement the `save` method to handle the logic for saving data in the new format.

   ```python src/strategy/save_as_html.py
   # src/strategy/save_as_html.py

   from .base import ISaveStrategy
   import pandas as pd

   class SaveAsHTML(ISaveStrategy):
      def save(self, data: pd.DataFrame, filename: str) -> None:
         if not isinstance(data, pd.DataFrame):
               raise ValueError("SaveAsHTML strategy is only applicable to Pandas DataFrame objects")
         data.to_html(f'{filename}.html')
         print(f'[strt][html] Saved {type(data).__name__} as HTML to {filename}.html')

   ```

3. Update the App Save Strategy List:

   - import the new save strategy into `src/app_save_strategy.py`

   ```python src/app_save_strategy.py
   # src/app_save_strategy.py

   # Existing Save Strategy...
   from src.strategy.save_as_html import SaveAsHTML
   ```

## Scenario 2: Adding a New Data Type

The system is designed to handle various data types and save them in different formats using save strategies. Adding a new data type involves creating a new class for the data type and integrating it into the system.

1. Create a New Data Type Class:

   - Navigate to the `data` folder.
   - Create a new Python file for the data type. Name the file according to the data type, e.g., `list_of_floats_data.py`.

2. Implement the New Data Type:

   - Define a new class in the created file.
   - Ensure that the new data type class inherits from `IData`.

   ```python src/data/list_of_floats_data.py
   # src/data/list_of_floats_data.py

   from .base import IData
   from typing import List

   class ListOfFloatsData(IData):
      def __init__(self, data: List[float]) -> None:
        if not all(isinstance(item, float) for item in data):
            raise ValueError("All elements must be float")
        super().__init__(data)
   ```

3. Modify the DataBuilder:

   -Add a corresponding method to the DataBuilder class to incorporate the new data type.

   ```python src/builder/data_builder.py
   # src/builder/data_builder.py

   # Add an import statement for the new data type
   from src.data.list_of_floats_data import ListOfFloatsData

   class DataBuilder:
      # Existing code...

      def create_list_of_floats(self, data: List[float]) -> 'DataBuilder':
         self._data = ListOfFloatsData(data)
         return self

      # Existing code...

   ```

4. Update the Save Strategies (if necessary):

   - Ensure that the existing save strategies can handle the new data type.
   - If special handling is required, modify the relevant strategy classes.

   ```python
   class SaveAsText(ISaveStrategy):
      def save(self, data: Any, filename: str) -> None:
         with open(f"output/{filename}.txt", "w") as file:
            if isinstance(data, list):
               print(f"[strt][txt] list saving...")
               file.write("\n".join([str(ele) for ele in data]))
            else:
               print(f"[strt][txt] default saving...")
               file.write(str(data))
            print(f"[strt][txt] Saved {type(data).__name__} as Text to {filename}.txt")
   ```

## Scenario 3: Adding a New Decorator

Decorators are used to dynamically add additional functionalities to save strategies, such as modifying the header, adjusting filenames, or any other pre/post-processing steps.

1. Create a New Decorator Class:

   - Navigate to the `decorator` folder.
   - Create a new Python file for the decorator. Name the file according to the functionality of the decorator, e.g., `save_with_timestamp.py`.

2. Implement the New Decorator:

   - Define a new class in the created file.
   - Ensure that the new decorator class inherits from `ISaveDecorator`.
   - Implement the `save` method to add the desired functionality before or after calling the base save strategy's save method.

   ```python src/decorator/save_with_timestamp.py
   # src/decorator/save_with_timestamp.py

   from datetime import datetime
   from typing import Any
   from src.strategy.base import ISaveStrategy
   from .base import ISaveDecorator

   class SaveWithTimestamp(ISaveDecorator):
      def __init__(self, strategy: ISaveStrategy) -> None:
         super().__init__(strategy)

      def save(self, data: Any, filename: str) -> None:
         timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
         filename = f"{filename}_{timestamp}"
         print(f'Appending timestamp to filename: {filename}')
         self._strategy.save(data, filename)
   ```

3. Modify the `DataBuilder` if Necessary:

   - Add a corresponding method to the `DataBuilder` class to incorporate the new decorator.

   ```python src/builder/data_builder.py
   # src/builder/data_builder.py

   # Add an import statement for the new decorator
   from src.decorator.save_with_timestamp.py import SaveWithTimestamp

   class DataBuilder:
      # Existing code...

      def with_timestamp(self) -> 'DataBuilder':
         if not self._strategy:
               raise ValueError("Save strategy not set")
         self._strategy = SaveWithTimestamp(self._strategy)
         return self

      # Existing code...
   ```
