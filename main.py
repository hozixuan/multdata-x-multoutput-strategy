# main.py

import pandas as pd
import numpy as np

## Import the necessary thing
from src.app_api import DataBuilder
from src.app_save_strategy import (
    SaveAsCSV,
    SaveAsExcel,
    SaveAsJSON,
    SaveAsPDF,
    SaveAsText,
)


pdf_strategy = SaveAsPDF()
excel_strategy = SaveAsExcel()
text_strategy = SaveAsText()
csv_strategy = SaveAsCSV()
json_strategy = SaveAsJSON()

# Case 1: Save data frame as numpy
# Create a Pandas DataFrame and save it as Excel with a header and for a specific client
print(f"\n(main) ===== case1 =====")
df_data = (
    DataBuilder()
    .create_pandas_dataframe(pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6]}))
    .with_save_strategy(excel_strategy)
    .with_header("Report")
    .for_client("ClientA")
    .build()
)
df_data.save("dataframe_excel")

# Case 2: Save string data
# Create a string and save it as text for a specific client
print(f"\n(main) ===== case2 =====")
string_data = (
    DataBuilder()
    .create_string("Hello, World!")
    .with_save_strategy(text_strategy)
    .for_client("ClientB")
    .build()
)
string_data.save("string_data")

# Case 3: Save the numpy data
# Create a numpy array and save it as PDF (pseudocode) with a header
print(f"\n(main) ===== case3 =====")
numpy_data = (
    DataBuilder()
    .create_numpy_array(np.array([1, 2, 3]))
    .with_save_strategy(pdf_strategy)
    .with_header("Array Report")
    .build()
)
numpy_data.save("numpy_data")

# Case 4: Save the list of string data
# Create a list of strings and save it as text
print(f"\n(main) ===== case4 =====")
list_of_strings_data = (
    DataBuilder()
    .create_list_of_strings(["hello", "world", "example"])
    .with_save_strategy(text_strategy)
    .build()
)
list_of_strings_data.save("list_of_strings")

# Case 5: Save the json data
# Create a Pandas DataFrame and save it as JSON
print(f"\n(main) ===== case5 =====")
df_data_json = (
    DataBuilder()
    .create_pandas_dataframe(pd.DataFrame({"x": [10, 20], "y": [30, 40]}))
    .with_save_strategy(json_strategy)
    .build()
)
df_data_json.save("dataframe_json")

# Case 6: Save the json data, same but with text strategy
print(f"\n(main) ===== case6 =====")
df_data_json = (
    DataBuilder()
    .create_pandas_dataframe(pd.DataFrame({"x": [10, 20], "y": [30, 40]}))
    .with_save_strategy(text_strategy)
    .build()
)
df_data_json.save("dataframe_json")
