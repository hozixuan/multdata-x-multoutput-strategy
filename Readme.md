# MultData X MultOutput Strategy

With just multiple output problem, it is very easy to implement a design pattern to deal with it. But what if there is new type of data wanna to save? Could the saving module able to reuse back for the new data? How much changes is needed when new data type is add in? What if we need to output same data type but require to customize the output to cater for different client taste?

Hence, to answer these question, this repo exist. It try to handle multiple output, datatype, client tweak with design pattern to achieve easy to maintain, extend, slight customizable on the output in mind. It is not the perfect solution, there are still many way to improve it, hopefully this project repository as a kickstarter to make one able to generate more report they want~

## Getting start

Let's try demo with the script to have a taste how it look like!

1. install require library from `requirements.txt`
2. run the script `python3 main.py`

> Note: the author test run on ubuntu python 3.10.12

You will notice there willl have 6 output file generated at `output/`, they are generated based on the cases as written in `main.py`, you may refer to the command line and find back where is the implementation.

```sh
$ python3 main.py

(main) ===== case1 =====
[deco][client] Adjusting filename for client: ClientA_dataframe_excel
[deco][header] Adding header: Report
[strt][excel] Saved DataFrame as Excel to ClientA_dataframe_excel.xlsx

(main) ===== case2 =====
[deco][client] Adjusting filename for client: ClientB_string_data
[strt][txt] Saved str as Text to ClientB_string_data.txt

(main) ===== case3 =====
[deco][header] Adding header: Array Report
[strt][pdf] Saving ndarray as PDF to numpy_data.pdf

(main) ===== case4 =====
[strt][txt] Saved list as Text to list_of_strings.txt

(main) ===== case5 =====
[strt][json] Saved DataFrame as JSON to dataframe_json.json

(main) ===== case6 =====
[strt][txt] Saved DataFrame as Text to dataframe_json.txt
```

The cli output showing which piece of code or function it was calling to

- `[deco]`: The decorator module located in `src/decorator`
- `[strt]`: The strategy module located in `src/strategy`
- `(main)`: This line come from `main.py`

For example `[deco][client]` means the `save_for_client.py` in `src/decorator`, `[strt][txt]` means the `save_as_text.py` in `src/strategy`

**Code Analysis:**

Let take alook on `main.py` and see how it work!

```python
## Import the necessary thing
from src.app_api import DataBuilder
from src.app_save_strategy import (
    SaveAsCSV,
    SaveAsExcel,
    SaveAsJSON,
    SaveAsPDF,
    SaveAsText,
)
```

`DataBuilder` is the must to import, it build the data for later saving
Those saving strategy define what kind of export format to output.

and we gonna dive into `case1`

```python
print(f"\n(main) ===== case1 =====")
df_data = (
    DataBuilder()
    .create_pandas_dataframe(pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6]}))
    .with_save_strategy(excel_strategy)
    .with_header("Report")
    .for_client("ClientA")
    .build()
)
df_data.save("dataframe_excel")
```

This code uses a builder pattern to construct a data object (a Pandas DataFrame) and specifies how it should be saved. Here’s a step-by-step explanation:

- `DataBuilder()`: Starts the process of building a data object.
- `create_pandas_dataframe`: Creates a Pandas DataFrame with specified columns and data.
- `with_save_strategy`: Sets the saving strategy to Excel format.
- `with_header`: Adds a header titled "Report" to the data.
- `for_client`: Labels the file for "ClientA".
- `build()`: Finalizes the building of the data object.
- `save`: Saves the built data object to a file named "dataframe_excel" in Excel format.
